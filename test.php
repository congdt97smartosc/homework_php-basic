<?php
class circle{
    public $r;
    public function __construct($r){
        $this->r = $r;
    }
    //function to excute Circumference
    public function calcuCircleC(){
        $result = ($this->r * 2) * 3.14;
        $result2 = number_format($result, 2);
        return $result2;
    }
    //function to excute Acreage
    public function calcuCircleS(){
        $result = ($this->r * $this->r) * 3.14;
        $result2 = number_format($result, 2);
        return $result2;
    }
}
//validation
if(isset($argv[1])){
    $circle = new circle($argv[1]);
    $checkDigit = $argv[1];
    //validation 
    if(is_numeric($checkDigit) && !is_null($checkDigit) && ($checkDigit > 0)){
        echo "is number";
        echo "\nCircumference is: ";
        echo $circle->calcuCircleC();
        echo "\nAcreage is: ";
        echo $circle->calcuCircleS();
        echo "\n";
    }else{
        echo "please!!! input the valid number (r > 0, r is digit)\n";
    } 
}else{
    echo "Please input the value!! \n";
}

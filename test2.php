<?php
//open file random.txt
$randomFile = fopen("random.txt", "r") or die("Unable to open file!");
//convert text to array
$textToArray = preg_split("/[\s,]+/",fread($randomFile,filesize("random.txt")));
//Count the repeat time of the value
$countRepeatTime = array_count_values($textToArray);
//declare a variable to store the data under array type
$number_count = array();
//use foreach to check all the data in random.txt
foreach ($countRepeatTime as $key=>$value){
    // the condition >= 3
    if($value >= 3){
        //insert the correct value into the variable we have created above
        array_push($number_count,$key);
    }
}
//use IMPLODE to format the values and show them
$result = implode(',',$number_count);
echo 'the number repeat >= three time:'.$result;
echo "\n";